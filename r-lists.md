Get the names of an R list and print them out.

```r
  code <- '
    Robj printListNames(Robj rl) {
      Robj names = getAttrib(rl, "names");
      printR(names);
      return RNil;
    }
  '
  compileD("test1", code)
  z <- list(var1=5, var2=10)
  .Call("printListNames", z)
```

Alternatively, you can do

```r
  code2 <- '
    Robj printListNames2(Robj rl) {
      printR(RList(rl).names);
		  return RNil;
    }
  '
  compileD("test2", code2)
  z <- list(var1=5, var2=10)
  .Call("printListNames2", z)
```

or

```r
  code3 <- '
    Robj printListNames3(Robj rl) {
      printR(rl.rlist.names);
		  return RNil;
    }
  '
  compileD("test3", code3)
  z <- list(var1=5, var2=10)
  .Call("printListNames3", z)
```