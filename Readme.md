**Note:** This package is here only for legacy purposes. The new version of this package can be found [here](https://bitbucket.org/bachmeil/dmdinline2).

An R package that allows inline D functions to be inserted in R code. Currently provides:

- A function that compiles D code, creates a shared library, and loads it into R
- A function that installs librtod2

## Dependencies

Requires libgretl and R. If you are interested in this package, you will already have R installed. To install libgretl (the development library, not the desktop application) on Ubuntu/Mint, you can do

```
sudo apt-get install libgretl1-dev
```

## Installation

I recommend using devtools:

``` r
library(devtools)
install_bitbucket("bachmeil/dmdinline")
```

You will also need an installation of librtod2. The easy way to do that is with the provided function:

```r
library(dmdinline)
install_librtod2(gretl="usr/lib/libgretl-1.0.so", libr="usr/lib/R/lib/libR.so")
```

You will need to replace the two arguments with the locations of libgretl and libR. If you are using Ubuntu/Mint, the values above will probably work.

## Example

In R:

``` r
library(dmdinline)
txt <- '
Robj testfun(Robj rx, Robj ry) {
  double x = rx.scalar;
  double y = ry.scalar;
  double z = x+y;
  return z.robj;
}'
compileD("foo", txt)
.Call("testfun", 3.5, -2.6)
```

## Limitations

I've only tested it on Linux using DMD 2.065. There's no reason it shouldn't work with another OS or version of DMD but that's what I use.