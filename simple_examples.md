Take an R vector as an argument and return a new R vector that multiplies every element by a scalar.

```r
Robj scale(Robj rv, Robj rz) {
	RVector v = rv.vec;
	double z = rz.scalar;
	auto result = RVector(v.length);
	foreach(obs; 0..v.length) { result[obs] = z*v[obs]; }
	return result.robj;
}
```

It might be more fun to work with D's dynamic arrays. An equivalent (but slower) version of the above function is

```r
Robj scale2(Robj rv, Robj rz) {
	RVector v = rv.vec;
  double z = rz.scalar;
  double[] result;
  foreach(val; v) { result ~= z*val; }
  return result.vec.robj; // creates a vector in R and copies result into it
}
```

Note that foreach can be used with an RVector. The combination of a D array to hold the result plus foreach over the elements of v frees you from doing any indexing or having to calculate the length of the result.
