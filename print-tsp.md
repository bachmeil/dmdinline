Work with/change the `tsp` attributes of an R ts object. Most output can be handled using std.stdio. That is not the case with an R object, for which you use `printR`.

```r
  Robj printTsp(Robj ry) {
		Robj tsp = getAttrib(ry, "tsp");
		printR(tsp);
		return RNil;
	}
```

R's .Call interface requires all functions to return something. Returning `RNil` is the equivalent of a void function.

```r
  Robj changeTsp(Robj ry) {
		auto tsp = getAttrib(ry, "tsp").vec;
		tsp[0] = tsp[0] + 1;
		tsp[1] = tsp[1] + 1;
		setAttrib(ry, "tsp", tsp.robj);
		return RNil;
	}
```

`changeTsp` is destructive - it changes the tsp attributes of the R object.