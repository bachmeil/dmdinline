
module rtod2.matrix;
import rtod2.gretl, rtod2.r, rtod2.utils, rtod2.vector;
import std.exception, std.array, std.conv, std.string;

private struct matrix_info {
  int t1;
  int t2;
  char **colnames;
  char **rownames;
}

struct DoubleMatrix {
  int rows; // int needed for compatibility with the C libraries
  int cols;
  double * val;
  matrix_info * info;

  this(int r, int c) {
    rows = r;
    cols = c;
    auto temp = new double[to!int(r*c)];
    val = temp.ptr;
  }    

  // Warning: Does not copy, so any changes will affect the DoubleVector as well
  // Send v.dup as an argument if you want a copy
  // Speed gets priority as this could be called many times
  this(DoubleVector v) {
    cols = 1;
    rows = v.length;
    val = v.data;
  }

  // Warning: does not copy, so any changes will affect the double[] as well
  // Send x.dup as an argument if you want a copy
  // Speed gets priority as this could be called many times
  this(double[] x) {
    rows = to!int(x.length);
    cols = 1;
    val = x.ptr;
  }

  this(Appender!(double[]) x) { 
    DoubleMatrix(x.data); 
  }

  // Copies
  this(Row[] rs) {
    rows = to!int(rs.length);
    cols = to!int(rs[0].length);
    auto result = DoubleMatrix(to!int(rs.length), to!int(rs[0].length));
    foreach(ii; 0..to!int(rs.length)) { result.setRow(ii, rs[ii]); }
    val = result.val;
  }

  // Copies
  this(Col[] cs) {
    rows = to!int(cs[0].length);
    cols = to!int(cs.length);
    auto result = DoubleMatrix(to!int(cs[0].length), to!int(cs.length));
    foreach(ii; 0..to!int(cs.length)) { result.setCol(ii, cs[ii]); }
    val = result.val;
  }

  // Copies
//   this(Appender!(Row[]) rsApp) {
//     auto rs = rsApp.data;
//     rows = to!int(rs.length);
//     cols = to!int(rs[0].length);
//     auto result = DoubleMatrix(to!int(rs.length), to!int(rs[0].length));
//     foreach(ii; 0..to!int(rs.length)) { result.setRow(ii, rs[ii]); }
//     val = result.val;
//   }

  // Copies
//   this(Appender!(Col[]) csApp) {
//     auto cs = csApp.data;
//     rows = to!int(cs[0].length);
//     cols = to!int(cs.length);
//     auto result = DoubleMatrix(to!int(cs[0].length), to!int(cs.length));
//     foreach(ii; 0..to!int(cs.length)) { result.setCol(ii, cs[ii]); }
//     val = result.val;
//   }

  double opIndex(int r, int c) {
    return val[c*rows+r];
  }

  void opIndexAssign(double v, int r, int c) {
    val[c*rows+r] = v;
  }

  // * means matrix multiplication, not element-by-element multiplication
  DoubleMatrix opBinary(string op)(DoubleMatrix y) {
    static if (op == "+") {
      assertR(this.rows == y.rows, "Different number of rows in matrix addition"); 
      assertR(this.cols == y.cols, "Different number of columns in matrix addition");
      auto result = DoubleMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] + y.val[ii]; }
      return result;
    }
    static if (op == "-") {
      assertR(this.rows == y.rows, "Different number of rows in matrix subtraction"); 
      assertR(this.cols == y.cols, "Different number of columns in matrix subtraction");
      auto result = DoubleMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] - y.val[ii]; }
      return result;
    } 
    static if (op == "*") {
      assertR(this.cols == y.rows, "Dimensions do not match for matrix multiplication");
      auto result = DoubleMatrix(this.rows, y.cols);
      gretl_matrix_multiply(this.ptr, y.ptr, result.ptr);
      return result;
    }
  }

  // Can treat a vector as a matrix if the dimensions are right
  DoubleMatrix opBinary(string op)(DoubleVector v) {
    static if (op == "+") { return this + DoubleMatrix(v); }
    static if (op == "-") { return this - DoubleMatrix(v); }
    static if (op == "*") { return this * DoubleMatrix(v); }
  }

  DoubleMatrix opBinaryRight(string op)(DoubleVector v) {
    static if (op == "+") { return DoubleMatrix(v) + this; }
    static if (op == "-") { return DoubleMatrix(v) - this; }
    static if (op == "*") { return DoubleMatrix(v) * this; }
  }

  // Scalar operations - now have division as well, because it's unambiguous
  GretlMatrix opBinary(string op)(double y) {
    static if (op == "+") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] + y; }
      return result;
    }
    static if (op == "-") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] - y; }
      return result;
    }
    static if (op == "*") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] * y; }
      return result;
    }
    static if (op == "/") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] / y; }
      return result;
    }
  }

  DoubleMatrix opBinaryRight(string op)(double y) {
    static if (op == "+") { return this+y; }
    static if (op == "-") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = y - this.val[ii]; }
      return result;
    }
    static if (op == "*") { return this*y; }
    static if (op == "/") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = y / this.val[ii]; }
      return result;
    }
  }

  DoubleMatrix * ptr() { return &this; }
}

GretlMatrix dup(DoubleMatrix m) {
  auto result = GretlMatrix(m.rows, m.cols);
  foreach(obs; 0..m.rows*m.cols) { result.val[obs] = m.val[obs]; }
  return result;
}

void setColumn(DoubleMatrix x, int col, DoubleVector src) {
  assertR(x.rows == src.length, "In setColumn: Number of rows in matrix do not equal length of vector");
  foreach(ii; 0..x.rows) {
    x[ii, col] = src[ii];
  }
}

void fillColumn(DoubleMatrix x, int col, double val) {
  foreach(ii; 0..x.rows) {
    x[ii, col] = val;
  }
}

struct IntMatrix {
  int rows;
  int cols;
  int * val;
  matrix_info * info;

  this(int r, int c) {
    rows = r;
    cols = c;
    auto temp = new int[to!int(r*c)];
    val = temp.ptr;
  }    

  // Warning: Does not copy, so any changes will affect the DoubleVector as well
  // Send v.dup as an argument if you want a copy
  this(IntVector v) {
    cols = 1;
    rows = v.length;
    val = v.data;
  }

  // Warning: does not copy, so any changes will affect the double[] as well
  // Send x.dup as an argument if you want a copy
  this(int[] x) {
    rows = to!int(x.length);
    cols = 1;
    val = x.ptr;
  }

  this(Appender!(int[]) x) { IntMatrix(x.data); }

  int opIndex(int r, int c) {
    return val[c*rows+r];
  }

  void opIndexAssign(int v, int r, int c) {
    val[c*rows+r] = v;
  }

  IntMatrix opBinary(string op)(IntMatrix y) {
    static if (op == "+") {
      assertR(this.rows == y.rows, "Different number of rows in matrix addition"); 
      assertR(this.cols == y.cols, "Different number of columns in matrix addition");
      auto result = IntMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] + y.val[ii]; }
      return result;
    }
    static if (op == "-") {
      assertR(this.rows == y.rows, "Different number of rows in matrix subtraction"); 
      assertR(this.cols == y.cols, "Different number of columns in matrix subtraction");
      auto result = IntMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] - y.val[ii]; }
      return result;
    } 
  }

  // Can treat a vector as a matrix if the dimensions are right
  IntMatrix opBinary(string op)(IntVector v) {
    static if (op == "+") { return this + IntMatrix(v); }
    static if (op == "-") { return this - IntMatrix(v); }
  }

  IntMatrix opBinaryRight(string op)(IntVector v) {
    static if (op == "+") { return IntMatrix(v) + this; }
    static if (op == "-") { return IntMatrix(v) - this; }
  }

  // Scalar operations - now have division as well, because it's unambiguous
  IntMatrix opBinary(string op)(int y) {
    static if (op == "+") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] + y; }
      return result;
    }
    static if (op == "-") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] - y; }
      return result;
    }
    static if (op == "*") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] * y; }
      return result;
    }
    static if (op == "/") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] / y; }
      return result;
    }
  }

  IntMatrix opBinaryRight(string op)(int y) {
    static if (op == "+") { return this+y; }
    static if (op == "-") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = y - this.val[ii]; }
      return result;
    }
    static if (op == "*") { return this*y; }
    static if (op == "/") {
      auto result = this.dup;
      foreach(ii; 0..rows*cols) { result.val[ii] = y / this.val[ii]; }
      return result;
    }
  }

  IntMatrix * ptr() { return &this; }
}

DoubleMatrix copyRows(DoubleMatrix m, int r) {
  assertR(r < m.rows, "Row index of matrix is out of bounds");
  auto result = DoubleMatrix(1, m.cols);
  foreach(col; 0..m.cols) { result[0,col] = m[r, col]; }
  return result;
}

DoubleMatrix copyRows(DoubleMatrix m, int[] rows) {
  auto result = DoubleMatrix(to!int(rows.length), m.cols);
  foreach(ii; 0..to!int(rows.length)) { 
    Row(result, ii) = Row(m, ii);
  }
  return result;
}  

DoubleMatrix copyRows(DoubleMatrix m, int r0, int r1, int by=1) {
  assertR(r0 <= r1, "In copyRows: second index has to be after first");
  auto rows = seq(r0, r1-1, by);
  return(copyRows(m, rows));
} 

void copyRows(DoubleMatrix src, int r0, int r1, DoubleMatrix dest, int r2, int r3) {
  assertR(r0 <= r1, "In copyRows: second index has to be after the first");
  assertR(r2 <= r3, "In copyRows: third index has to be after the fourth");
  assertR(r3 - r2 == r1 - r0, "In copyRows: must be copying the same number of rows");
  int diff = r2 - r0;
  foreach(ii; r2..r3) { Row(dest, ii) = Row(src, ii-diff); }
}

DoubleMatrix copyColumns(DoubleMatrix m, int c) {
  assertR(c < m.cols, "Column index of matrix is out of bounds");
  auto result = DoubleMatrix(m.rows, 1);
  foreach(row; 0..m.rows) { result[row,0] = m[row,c]; }
  return result;
}

DoubleMatrix copyColumns(DoubleMatrix m, int[] cols) {
  auto result = DoubleMatrix(m.rows, to!int(cols.length));
  foreach(ii; 0..to!int(cols.length)) { 
    Col(result, ii) = Col(m, ii);
  }
  return result;
}  

DoubleMatrix copyColumns(DoubleMatrix m, int c0, int c1, int by=1) {
  auto cols = seq(c0, c1-1, by);
  return(copyColumns(m, cols));
}

void setRow(T)(DoubleMatrix m, int r, T data) {
  foreach(ii; 0..data.length) { m[r,ii] = data[ii]; }
}

void setCol(T)(DoubleMatrix m, int c, T data) {
  foreach(ii; 0..data.length) { m[ii,c] = data[ii]; }
}

void fill(DoubleMatrix m, double z) { 
  foreach(row; 0..m.rows*m.cols) { m.val[row] = z; } 
}

GretlMatrix chol(DoubleMatrix m) {
  GretlMatrix result = m.dup();
  int err = gretl_matrix_cholesky_decomp(result.ptr);
  assertR(err == 0, "Cholesky decomposition failed");
  return result;
}

GretlMatrix inv(DoubleMatrix m) {
  GretlMatrix result = m.dup();
  int err = gretl_invert_matrix(result.ptr);
  assertR(err == 0, "Taking the inverse of a matrix failed");
  return result;
}

DoubleMatrix transpose(DoubleMatrix m) {
  auto result = DoubleMatrix(m.cols, m.rows);
  int err = gretl_matrix_transpose(result.ptr, m.ptr);
  assertR(err == 0, "Taking the transpose of a matrix failed");
  return result;
}

double det(DoubleMatrix m) {
  GretlMatrix temp = m.dup();
  int err;
  double result = gretl_matrix_determinant(temp.ptr, &err);
  assertR(err == 0, "Taking the determinant of a matrix failed");
  return result;
}

double logdet(DoubleMatrix m) {
  GretlMatrix temp = m.dup();
  int err;
  double result = gretl_matrix_log_determinant(temp.ptr, &err);
  assert(err == 0, "Taking the log determinant of a matrix failed");
  return result;
}

DoubleMatrix diag(DoubleMatrix m) {
  assertR(m.rows == m.cols, "diag is not intended to take the diagonal of a non-square matrix");
  auto result = DoubleMatrix(m.rows, 1);
  foreach(ii; 0..m.rows) { result[ii,0] = m[ii,ii]; }
  return result;
}

void setDiagonal(DoubleMatrix m, DoubleMatrix newdiag) {
  assertR((newdiag.cols == 1) | (newdiag.rows == 1), "Cannot set a diagonal to a matrix");
  assertR(m.rows == m.cols, "Setting the diagonal of a non-square matrix");
  if (newdiag.cols == 1) {
    assertR(m.rows == newdiag.rows, "Wrong number of elements in the new diagonal");
    foreach(ii; 0..newdiag.rows) { m[ii,ii] = newdiag[ii,0]; }
  } else {
    assertR(m.cols == newdiag.cols, "Wrong number of elements in the new diagonal");
    foreach(ii; 0..newdiag.cols) { m[ii,ii] = newdiag[0,ii]; }
  }
}

void setDiagonal(T)(DoubleMatrix m, T v) {
  assertR(m.rows == m.cols, "Setting the diagonal of a non-square matrix");
  assertR(m.rows == v.length, "Wrong number of elements in the new diagonal");
  foreach(ii; 0..v.length) { m[ii,ii] = v[ii]; }
}

void setDiagonal(DoubleMatrix m, double v) {
  assertR(m.rows == m.cols, "Attempting to set the diagonal of a non-square matrix");
  foreach(ii; 0..m.rows) { m[ii,ii] = v; }
}

double trace(DoubleMatrix m) { 
  assertR(m.rows == m.cols, "Attempting to take the trace of a non-square matrix");
  return gretl_matrix_trace(m.ptr); 
}

GretlMatrix raise(DoubleMatrix m, double k) {
  GretlMatrix result = m.dup();
  gretl_matrix_raise(result.ptr, k);
  return result;
}

GretlMatrix pow(DoubleMatrix m, int s) {
  assertR(s >= 0, "Exponent on matrix has to be a positive integer");
  assertR(m.rows == m.cols, "Attempting to exponentiate a non-square matrix. Use raise to raise each element of a matrix to the same power.");
  int err;
  auto temp = gretl_matrix_pow(m.ptr, s, &err);
  assertR(err == 0, "Exponentiating a matrix failed");
  return GretlMatrix(temp);
}

DoubleMatrix solve(DoubleMatrix x, DoubleMatrix y) {
  auto temp = x.dup();
  auto result = y.dup();
  int err = gretl_LU_solve(temp.ptr, result.ptr);
  assertR(err == 0, "Call to solve failed");
  return result;
}

DoubleMatrix eye(int k) {
  auto result = DoubleMatrix(k, k);
  result.fill(0.0);
  foreach(ii; 0..k) { result[ii,ii] = 1.0; }
  return result;
}

DoubleMatrix kron(DoubleMatrix x, DoubleMatrix y) {
  auto result = DoubleMatrix(x.rows*y.rows, x.cols*y.cols);
  int err = gretl_matrix_kronecker_product(x.ptr, y.ptr, result.ptr);
  assertR(err == 0, "Kronecker product failed");
  return result;
}

private struct Observation {
  double value;
  int obs;
}

DoubleMatrix sort(bool inc=true)(DoubleMatrix m, int col) {
  // Put all elements and row numbers in an array
  auto temp = new Observation[m.rows];
  foreach(ii; 0..m.rows) { temp[ii] = Observation(m[ii,col], ii); }

  // Sort the array according to data
  static if (inc) {
    bool com(Observation x, Observation y) { return x.value < y.value; }
  } else {
    bool com(Observation x, Observation y) { return x.value > y.value; }
  }
  auto output = sort!(com)(temp);

  // Now put the results into a new matrix
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows) { Row(result, ii) = Row(m, output[ii].value); }
  return result;
}

int[] order(bool inc=true)(DoubleMatrix m, int col) {
  auto temp = new Observation[m.rows];
  foreach(obs; 0..m.rows) { temp[obs] = Observation(m[obs,col], obs); }

  // Sort the array according to data
  static if (inc) {
    bool com(Observation x, Observation y) { return x.value < y.value; }
  } else {
    bool com(Observation x, Observation y) { return x.value > y.value; }
  }
  auto output = sort!(com)(temp);
  return output.array;
}

struct GretlMatrix {
  DoubleMatrix mat;
  DoubleMatrix * ptr;
  
  this(DoubleMatrix * gm) {
    mat.rows = gm.rows;
    mat.cols = gm.cols;
    mat.val = gm.val;
    mat.info = gm.info;
    ptr = gm;
  }

  this(int r, int c = 1) {
    ptr = gretl_matrix_alloc(r, c);
    mat.rows = r;
    mat.cols = c;
    mat.val = ptr.val;
    mat.info = ptr.info;
  }

  double * dataptr() {
    return mat.val;
  }

  ~this() { 
    gretl_matrix_free(ptr); 
  }

  alias mat this;
}

struct Col {
  DoubleMatrix mat;
  int col;
  double * data;
  int length;

  this(DoubleMatrix m, int c) {
    mat = m;
    col = c;
    data = m.val;
    length = m.rows;
  }

  double opIndex(int r) {
    assertR(r < length, "Col index out of bounds");
    return data[r];
  }

  void opIndexAssign(double val, int ii) {
    data[ii] = val;
  }

  void opAssign(T)(T v) {
    assert(this.length == v.length, "Attempting to copy into Col an object with the wrong number of elements");
    foreach(ii; 0..to!int(this.length)) { this[ii] = v[ii]; }
  }
  
  void opAssign(double x) {
    foreach(ii; 0..to!int(this.length)) { this[ii] = x; }
  }

  bool empty() { return length == 0; }
  double front() { return data[0]; }
  void popFront() {
    data = &data[1];
    length -= 1;
  }
}

double[] array(Col col) {
  double[] result;
  result.reserve(col.length);
  foreach(val; col) { result ~= val; }
  return result;
}

Robj robj(Col[] x) {
  auto result = RMatrix(to!int(x[0].length), to!int(x.length));
  foreach(ii; 0..to!int(x.length)) {
    Col(result, ii) = x[ii];
  }
  return result.robj;
}

Robj robj(Row[] x) {
  auto result = RMatrix(to!int(x.length), to!int(x[0].length));
  foreach(ii; 0..to!int(x.length)) {
    Row(result, ii) = x[ii];
  }
  return result.robj;
}

// Robj robj(Appender!(Col[]) xapp) {
//   return xapp.data.robj;
// }
//   
// Robj robj(Appender!(Row[]) xapp) {
//   return xapp.data.robj;
// }

struct Row {
  DoubleMatrix mat;
  int row;
  double * data;
  int length;

  this(DoubleMatrix m, int r) {
    mat = m;
    row = r;
    data = m.val;
    length = m.cols;
  }

  double opIndex(int c) {
    assertR(c < length, "Row index out of bounds");
    return data[c*mat.rows+row];
  }

  void opIndexAssign(double val, int ii) {
    data[ii] = val;
  }

  void opAssign(T)(T v) {
    assertR(this.length == v.length, "Attempting to copy into Row an object with the wrong number of elements");
    foreach(ii; 0..to!int(this.length)) { this[ii] = v[ii]; }
  }

  void opAssign(double x) {
    foreach(ii; 0..to!int(this.length)) { this[ii] = x; }
  }

  bool empty() { return length == 0; }
  double front() { return data[0]; }
  void popFront() {
    data = &data[mat.rows];
    length -= 1;
  }
}

double[] array(Row row) {
  double[] result;
  result.reserve(row.length);
  foreach(val; row) { result ~= val; }
  return result;
}

struct ByColumn {
  DoubleMatrix mat;
  int colno;
  int length;

  this(DoubleMatrix m) {
    mat = m;
    colno = 0;
    length = m.cols;
  }

  bool empty() { return length == 0; }
  Col front() { return Col(mat, colno); }
  void popFront() {
    colno += 1;
    length -= 1;
  }
}

struct ByRow {
  DoubleMatrix mat;
  int rowno;
  int length;
  
  this(DoubleMatrix m) {
    mat = m;
    rowno = 0;
    length = m.rows;
  }

  bool empty() { return length == 0; }
  Row front() { return Row(mat, rowno); }
  void popFront() {
    rowno += 1;
    length -= 1;
  }
}

void print(DoubleMatrix m, string msg="") {
  gretl_matrix_print(m.ptr, toStringz(msg));
}
